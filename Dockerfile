FROM python:3
WORKDIR /usr/src/app
COPY . .
RUN apt-get update && apt-get install python3-pip -y
RUN pip3 install psycopg2
CMD python3 -m http.server --cgi

