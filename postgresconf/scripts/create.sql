-- Создание таблицы
CREATE TABLE IF NOT EXISTS accounts (
	user_id SERIAL PRIMARY KEY NOT NULL,
	name varchar(20) NOT NULL,
	password varchar(24) NOT NULL,
	time_create timestamp NOT NULL DEFAULT now());
